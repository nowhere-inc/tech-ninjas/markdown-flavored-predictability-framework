# frozen_string_literal: true

require 'date'
require 'terminal-table'
require_relative 'data_fetcher/lunch_breaks'
require_relative 'data_fetcher/worktime_boundaries'
require_relative 'analytics/weekly_boundaries'

# % DATA_SOURCE=git_repo_path ruby lib/achievements.rb

# |-------------------------------------|
# | Boundaries Points 3                 |
# | Boundaries Penalty: 2               |
# |-------------------------------------|
# |-------------------------------------|
# | Week #XX score 1 # (3 - 2)          |
# |-------------------------------------|

current_week_data = Analytics::WeeklyBoundaries.new # DATA_SOURCE should be given

# this is good candidate for presenter
rows = []
rows << :separator
rows << ["Boundaries Points #{current_week_data.day_start_points}"]
rows << ["Boundaries Penalty #{current_week_data.penalty_points}"]
rows << :separator
rows << :separator
rows << ["Week ##{Date.today.cweek} score: #{current_week_data.total_points}"]
rows << :separator
table = Terminal::Table.new rows: rows
table.style = { border: :markdown }
puts table
