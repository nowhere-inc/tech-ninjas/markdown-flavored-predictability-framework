# frozen_string_literal: true

module Analytics
  # Weekly
  class WeeklyBoundaries
    def total
      worktime_boundaries
    end

    def worktime_boundaries
      worktime_boundaries_fetcher.current_week
    end

    def day_start_points
      0
    end

    def day_finish_points
      0
    end

    def lunch_break_points
      0
    end

    def penalty_points
      0
    end

    def total_points
      lunch_break_points + day_start_points + day_finish_points - penalty_points
    end

    private

    def worktime_boundaries_fetcher
      DataFetcher::WorktimeBoundaries.new
    end
  end
end
