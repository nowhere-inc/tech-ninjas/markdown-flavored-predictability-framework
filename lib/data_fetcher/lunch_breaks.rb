# frozen_string_literal: true

module DataFetcher
  # LunchBreaks
  class LunchBreaks
    def current_week
      parsed_data[Date.today.cweek]
    end

    def parsed_data
      (0..Date.today.cweek).each_with_object(Hash.new(0)) do |week, hash|
        hash[week] = lunch_break_data.fetch(week, 0)
      end
    end

    private

    def lunch_break_data
      result = {}
      start_timestumps.each do |timestump|
        if finish_timestumps.include?(timestump)
          result[timestump.cweek] ||= 0
          result[timestump.cweek] += 1
        end
      end
      result
    end

    def start_timestumps
      raw_data_starts.map { |log_entry| parse_log_time(log_entry) }.map(&:to_date)
    end

    def finish_timestumps
      raw_data_finishes.map { |log_entry| parse_log_time(log_entry) }.map(&:to_date)
    end

    def raw_data_starts
      @raw_data_starts ||= `#{starts_cmd}`.split("\n")
    end

    def raw_data_finishes
      @raw_data_finishes ||= `#{finishes_cmd}`.split("\n")
    end

    def parse_log_time(log_entry)
      DateTime.parse(log_entry.match(/(?<=\().+?(?=\))/)[0])
    end

    # TODO: let's maybe move this code out of there?
    def starts_cmd
      <<~STARTS_CMD
        grep "start lunchbreak" spec/data/2023-02-02.log
      STARTS_CMD
    end

    def finishes_cmd
      <<~FINISHES_CMD
        grep "finish lunchbreak" spec/data/2023-02-02.log
      FINISHES_CMD
    end
  end
end
