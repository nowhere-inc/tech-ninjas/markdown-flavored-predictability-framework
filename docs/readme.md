# Welcome to the very basic ALKO practice!

Here are [historical requirements for ALKO LEVEL 0](https://gitlab.com/nowhere-inc/tech-ninjas/alkoframework/-/issues/30)

## Here is the guideline:
- use `git` and `GitLab`
- define your daily joy timeslot:
  - It can be morning or evening;
  - It can be 10 minutes or three hours.
- start your predefined slot on daily basis with `git commit --allow-empty -m "resume progress"`
- finish your predefined slot on daily basis with `git add .; git commit -am "pause progress"; git push`
- gather stats
- repeat

## What is `daily timeslot`?

Anything in between your predefined time boundaries. Maybe a walk? That should be fun! The FUTURAMA episode? Cooking borshcht? Rading the vim manual aloud? Duolinguo practice? Playing the guitar? You define!

## How should my `alko.yml` look like?


### basic  ALKO LEVEL 0 setup

```yaml
---
boundaries:
  status: ON
  reward: 3 points
```

### advansed ALKO LEVEL 0 setup

```yaml
---
worktime_boundaries:
  status: ON
  worktime_start: 9am
  worktime_finish: 4pm
  reward: 3 points
  penalty: 5 points
concsious_breaks:
  status: ON
  reward: 1 point
```
