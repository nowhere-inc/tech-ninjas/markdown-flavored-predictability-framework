# Welcome to the very basic practice!

## Here is the guideline:
- define your daily joy timeslot:
  - It can be morning or evening;
  - It can be 10 minutes or three hours.
- start your predefined slot on daily basis with `git commit --allow-empty 'start joy'`
- finish your predefined slot on daily basis with `git commit --allow-empty 'finish joy'`
- gather stats
- repeat

## What is `joy timeslot`?

Anything in between your predefined boundaries. Maybe a walk? That should be fun! The FUTURAMA episode? Cooking borshcht? Rading the vim manula aloud? Duolinguo practice?

## How should my `alko.yml` look like?


### basic setup

```yaml
---
boundaries: ON
  reward: 3 points
```

### advansed

```yaml
---
worktime_boundaries: ON
  worktime_start: 9am
  worktime_finish: 8pm
  reward: 3 points
  penalty: 5 points
concsious_breaks: ON
  reward: 1 point
```