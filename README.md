# ALKOframework

`ALKO` stands for creativity, curiosity, charm, friendliness, cheer and social life. Ref: http://www.meaningslike.com/name-stands-for/alko

aka `opinionated markdown-flavored personal predictability toolset`.

[some nore info is here](https://docs.google.com/presentation/d/1cEuswPgC1sUGndagKAyVYI1VSD9RrwTMD4Eg599Ld8k/edit?usp=sharing)

## Usage

```
gem install alkoframework

alko init

git add .

git commit -m "resume progress"

git push

```

```
gem install alko
cd <% your_gitlab_profile_special_repo %>
alko init
```


## Key Features:

- Tracking work boundaries and scores

## TODO: Planned Features:

- Pomodoro technique for daily view
- Personal OKRs for three week cadence
- work boundaries tracking ([ALKO LEVEL 0](https://gitlab.com/nowhere-inc/tech-ninjas/alkoframework/-/tree/main/docs/level_0))
- Focus box practice (ALKO LEVEL 1)
- Pomodoro technique (ALKO LEVEL 2)
- Productive Stimulation (ALKO LEVEL 3)
- Go Scrum Yourself (c)(tm) for weekly view (ALKO LEVEL 5)

## Data sources


```
petro@petro-laptop:~/joy/petrokoriakin1$ git log --format="%an (%ad): %h %s" --reverse -n 70 > /home/petro/joy/alkoframework/spec/data/2023-12-25.log
```

```
petro@petro-laptop:~/joy/petrokoriakin1$ cp -a alko-worklog/2023/cadence-18-dec-week-52-02 /home/petro/joy/alkoframework//spec/data/

```
