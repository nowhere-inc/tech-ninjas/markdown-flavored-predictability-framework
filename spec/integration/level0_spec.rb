# frozen_string_literal: true

require 'spec_helper'

describe 'level 0: basic boundaries' do
  subject(:script_output) { `ruby lib/achievements.rb` }

  it 'outputs something' do
    expect(script_output).not_to be_nil
  end
end
