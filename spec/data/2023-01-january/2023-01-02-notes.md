Pomodoro #3
=======
planning: day

Result
-------
- compose a plan for tomorrow
- ignore related discussions

Plan
-------
- compose a plan for tomorrow
- walk through all the comments again
- take a look into relative positioning decision and reasoning
- ignore related discussions

Details
-------
- the rabbit hole is too deep :(



Pomodoro #2
=======
clarifications: #367525

Result
-------
- walk through all the comments again

Plan
-------
- walk through all the comments again
- take a look into related discussions

Details
-------
- the rabbit hole is too deep :(



Pomodoro #1
=======
planning: week

Result
-------
- restructure notes
- compose last week todos

Plan
-------
- restructure notes
- compose last week todos


Details
-------


I know the Product: Today I achieved and learned
=======
- plans and expectations are in place!

Outputs and results of the day
=======
- my mood was: 5
- 45min afterlunch walk: DONE
- refined ton of processes with walks, internal team's responsibilities, etc

Plan for the day
=======
- absent

Extra items
=======
- please, configure gh, gd, search shortcuts
- please, wake up and follow your passion
- please, walk trough plan-related doc: check roadmap, check recordings

```
Pomodoro #0
=======


Result
-------


Plan
-------


Details
-------


```

```
git commit --allow-empty -m "resume progress"

git add . ;git commit -am "focusbox goal:"
git commit -am "focusbox result: 3"
git commit --allow-empty -m "finish focusbox"
git log --format="%an (%ad): %s" --reverse -n 5

git add . ;git commit -am "some results and plans"; git commit --allow-empty -m "pause progress"
git commit --allow-empty -m "Kick CI"; git push origin

tar -cvzf prod-2022-12-december.tgz ~/prod

tar -xvzf prod-2022-12-december.tgz
```


0. What is my goal for this week and the current state of my life vision.

1. What did achieve today?

2. What will I achieve tomorrow?

3. What is currently keeping me from making progress?
